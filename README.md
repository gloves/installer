# Glove Ansible Installer

基于 Ansible 的 [Glove](https://gitlab.com/gloves/glove) 安装器，目前支持对安装有 RHEL/CentOS 6-7 和 Debian/Ubuntu 操作系统的机器进行远程控制。![installer](assets/installer.png)

目前支持安装部署以下工具和软件：

1. [Glove](https://gitlab.com/gloves/glove) 部署工具（包括安装和执行测试）
2. MySQL 5.7.x
3. RabbitMQ 3.7.x



## 先决条件

### 操作系统

主控机器：除 Windows 之外的所有主流操作系统，推荐 CentOS 或者 Ubuntu Linux

目标机器：

- CentOS 6.x, CentOS 7.x
- RHEL 6.x, RHEL 7.x
- Oracle Linux 6.x, Oracle Linux 7.x
- Debian/Ubuntu

### 前置软件

| 机器                        | 软件            | 安装说明                                                     |
| --------------------------- | --------------- | ------------------------------------------------------------ |
| 主控机器（Control Machine） | python, Ansible | [Installing the Control Machine](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-the-control-machine) |
| 目标机器                    | python          | https://www.python.org/downloads/                            |



## 使用方法

```bash
cd /path/to/glove/installer
ansible-playbook -i hosts site.yaml
```

> 后面如无特殊说明，执行命令的主目录全部默认为 `/path/to/glove/installer/`
>

![installer demo](assets/demo.gif)

## 配置选项

### hosts 配置

在 `gloves` 组下修改对应的服务器的 IP 地址。关于 Ansible Host 的内置变量的说明，请访问以下链接：

https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#list-of-behavioral-inventory-parameters

hosts 文件示例：

```
[gloves]
192.168.180.100 ansible_user=root ansible_ssh_private_key_file=/path/to/private_key
192.168.180.101 ansible_user=root ansible_ssh_pass=secret
```



### group_vars 设置

组别 gloves 下的自定全局变量设置，在 `group_vars/gloves` 下，属性说明如下：

| 属性               | 默认值                              | 含义           |
| ------------------ | ----------------------------------- | -------------- |
| glove.install_path | /usr/local/share/glove              | Glove 安装目录 |
| glove.repo         | https://gitlab.com/gloves/glove.git | Glove 仓库地址 |
| glove.branch       | master                              | Glove 分支     |



## Ad-Hoc 任务

### 安装 MySQL 5.7

```bash
ansible-playbook -i hosts adhoc/install-mysql57.yaml
```



### 安装 RabbitMQ

```bash
ansible-playbook -i hosts adhoc/install-rabbitmq.yaml
```



### 运行 Glove 测试脚本

```bash
ansible-playbook -i hosts adhoc/run-test.yaml
```



